from unittest import TestCase

from src.knn_algorithm import KnnAlgorithm


class TestKnnAlgorithm(TestCase):
    def setUp(self):
        self.knn = KnnAlgorithm()

    def test_info_reset(self):
        """
        Test that info_reset function set valid value for test_info['nbhd_count']
        """
        test_info = {}
        self.knn.info_reset(test_info)
        self.assertEqual(test_info['nbhd_count'], 0)

    def test_save_new_data_as_df_2d(self):
        """
        Test that save_new_data_as_df_2d throw ValueError if given data is empty
        """
        given_data = {}
        self.knn.save_new_data_as_df_2d(given_data)
        self.assertRaises(ValueError)

    def test_dic_inc_with_given_key_none(self):
        """
        Test that dic_inc return False if given key is None
        """
        result = self.knn.dic_inc(None, {})
        self.assertFalse(result)

    def test_dic_inc_if_value_not_in_dict(self):
        """
        Test that dic_inc set value 1 if given key in given dic is None
        """
        given_dict = {'test': '2'}
        self.knn.dic_inc('given_key', given_dict)
        self.assertEqual(given_dict['given_key'], 1)
