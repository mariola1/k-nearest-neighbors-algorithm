# k-nearest neighbors algorithm

K-Nearest Neighbors is one of the simplest Machine Learning algorithms based on Supervised Learning technique.
K-NN algorithm assumes the similarity between the new case/data and available cases and put the new case into the category that is most similar to the available categories. More information about KNN Algorithm --> https://en.wikipedia.org/wiki/K-nearest_neighbors_algorithm

**k-nearest neighbors algorithm implementation with data visualization**

1. This program applies KNN algorithm to the two-dimensional data using the k-nearest neighbors with the Manhattan distance
2. Pandas library is used
3. To data vizualization matplotlib module is used



