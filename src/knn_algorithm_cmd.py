import argparse

from src.knn_algorithm import KnnAlgorithm
from src.temperature_preferences_graph import GraphDrawer

parser = argparse.ArgumentParser(description='Please, input as arguments:\n' +
                                             '1. the name of the data file with the 2d coordinates of the' +
                                             'points be applied on the knn algorithm.\n' +
                                             '2. the name of the output file where the classification of' +
                                             'the neighbours should be recorded.\n' +
                                             '3-6. the coordinates of the rectangle in which the class of' +
                                             'the neighbors should be determined:\n' +
                                             '3. x coordinate of the bottom left point of the ' +
                                             'rectangle,\n' +
                                             '4. x coordinate of the top right point of the ' +
                                             'rectangle,\n' +
                                             '5. y coordinate of the bottom left point of the ' +
                                             'rectangle,\n' +
                                             '6. y coordinate of the top right point of the ' +
                                             'rectangle.\n' +
                                             '7. the number of k-neighbors to consider in the' +
                                             'classification.\n' +
                                             '8. information if graph should be presented, "yes" \n'
                                             + 'if graph shuld be presented no otherwise.\n\n' +
                                             'Example use:\n' +
                                             'python knn_to_data.py mary_and_temperature_preferences.data' +
                                             ' mary_and_temperature_preferences_completed.data' +
                                             ' 5 30 0 10 1')

parser.add_argument("-input_data", required=True, help="the name of the data file with the 2d coordinates of the")
parser.add_argument("-output_data", required=True, help="the name of the output file where the classification of" +
                                                        "the neighbours should be recorded.\n")
parser.add_argument("-x_from", required=True, type=int, help="x coordinate of the bottom left point of the\n" +
                                                             "rectangle")
parser.add_argument("-x_to", required=True, type=int, help="x coordinate of the top right point of the" +
                                                           "rectangle")
parser.add_argument("-y_from", required=True, type=int, help="y coordinate of the bottom left point of the" +
                                                             "rectangle")
parser.add_argument("-y_to", required=True, type=int, help="y coordinate of the top right point of the" +
                                                           "rectangle")
parser.add_argument("-k", required=True, type=int, help="the number of k-neighbors to consider in the" +
                                                        "classification.")
parser.add_argument("-graph", required=True, help=" yes in case when graph should be presented " +
                                                  "no otherwise")

args = parser.parse_args()
FILE_PATH = args.input_data
OUTPUT_XLS_FILE = args.output_data
x_from = args.x_from
x_to = args.x_to
y_from = args.y_from
y_to = args.y_to
k = args.k
graph = args.graph

knn_alg = KnnAlgorithm()

learning_data = knn_alg.save_learning_data_as_dictionary_2d(FILE_PATH, ' ')
new_data = knn_alg.knn_algorithm(learning_data, x_from, x_to, y_from, y_to, k)
df = knn_alg.save_new_data_as_df_2d(new_data)
xls_file = df.to_excel(OUTPUT_XLS_FILE)

if graph == "yes":
    gd = GraphDrawer()
    gd.draw_graph('Wind', 'Temperature', df)
elif graph == "no":
    print("Graph is not presented")
else:
    print("Wrong value of -graph argument. yes or no only available")
