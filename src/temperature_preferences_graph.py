import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import pandas as pd


class GraphDrawer:
    def __init__(self, xlxs_file='../data/output/output_data.xlsx'):
        self.xlxs_file = xlxs_file
        self.data_frame = pd.read_excel(xlxs_file, index_col=0)

    def draw_graph(self, x_dim, y_dim, df=None):
        if df is None:
            df = self.data_frame
        x = df[x_dim]
        y = df[y_dim]
        df.loc[df["Classifier"].str.contains("cold"), "Classifier"] = "blue"
        df.loc[df["Classifier"].str.contains("warm"), "Classifier"] = "red"
        fig, ax = plt.subplots(figsize=(10, 5))
        ax.scatter(x, y, c=df['Classifier'], marker='o')
        plt.xlabel('Wind [km/h]')
        plt.ylabel('Temperature [C]')
        plt.title('Temperature Preferences')
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        ax.set_facecolor("lavender")
        ax.grid(color='grey', linestyle='-', linewidth=0.4, alpha=0.5)
        blue_patch = mpatches.Patch(color='blue', label='cold')
        red_patch = mpatches.Patch(color='red', label='warm')
        plt.legend(handles=[blue_patch, red_patch], loc='upper right')
        plt.show()


test = GraphDrawer()
test.draw_graph('Wind', 'Temperature')
