import pandas as pd


class KnnAlgorithm:
    def __init__(self, file_path='../data/mary_and_temperature_preferences',
                 output_path='../data/output/output_file.csv',
                 separator=' '):
        self.file_path = file_path
        self.output_path = output_path
        self.separator = separator

    def save_learning_data_as_dictionary_2d(self, path, separator):
        df = pd.read_csv(path, sep=separator, header=None)
        columns_names = ['X', 'Y', 'Classifier']
        df.columns = columns_names
        dic = df.set_index(['X', 'Y'])['Classifier'].to_dict()
        return dic

    def save_new_data_as_df_2d(self, data):
        df = pd.DataFrame(columns=['Wind', 'Temperature', 'Classifier'])
        for key, value in data.items():
            df = df.append({'Wind': key[0], 'Temperature': key[1], 'Classifier': value}, ignore_index=True)
        return df

    def info_reset(self, info):
        info['nbhd_count'] = 0
        info['class_count'] = {}

    def info_add(self, info, learning_data, x, y):
        group = learning_data.get((x, y), None)
        self.dic_inc(group, info['class_count'])
        info['nbhd_count'] += int(group is not None)

    def dic_inc(self, key, dic):
        if key is None:
            return False
        if dic.get(key, None) is None:
            dic[key] = 1
        else:
            dic[key] += 1

    def knn_algorithm(self, learning_data, x_from, x_to, y_from, y_to, k):
        info = {}
        new_data = {}
        for y in range(y_from, y_to + 1):
            for x in range(x_from, x_to + 1):
                self.info_reset(info)
                for dist in range(0, x_to - x_from + y_to - y_from + 1):
                    if dist == 0:
                        self.info_add(info, learning_data, x, y)
                    else:
                        [self.info_add(info, learning_data, x - i, y + dist - i) for i in range(0, dist)]
                        [self.info_add(info, learning_data, x + dist - i, y - i) for i in range(0, dist)]
                        [self.info_add(info, learning_data, x + i, y + dist - i) for i in range(1, dist)]
                        [self.info_add(info, learning_data, x - dist + i, y - i) for i in range(1, dist)]
                    if info['nbhd_count'] >= k:
                        break
                class_max_count = None

                for group, count in info['class_count'].items():
                    if group is not None and (class_max_count is None or count > info['class_count'][class_max_count]):
                        class_max_count = group
                new_data[x, y] = class_max_count
        return new_data
